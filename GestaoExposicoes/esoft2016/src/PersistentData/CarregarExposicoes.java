package PersistentData;

import Dominio.Exposicao;
import java.util.List;

public interface CarregarExposicoes {
    public List<Exposicao> carregaExposicoes();
}
