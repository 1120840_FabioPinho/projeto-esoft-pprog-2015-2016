package Dominio;

import Estados.DemonstracaoInicialState;
import Estados.DemonstracaoState;

public class Demonstracao {

    private DemonstracaoState estado;
    public Demonstracao() {
        estado = new DemonstracaoInicialState(this);
    }

    public void setEstado(DemonstracaoState novoEstado) {
        estado = novoEstado;
    }
    
}
