package Dominio;

import Estados.ExposicaoInicialState;
import Estados.ExposicaoState;
import Tarefas.Agendavel;
import Tarefas.Tarefa;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;

public class Exposicao extends Agendavel implements FaeAtribuivel{
    private String titulo;
    private String txtDescritivo;
    private Local local;
    private Date dataInicioExpo;
    private Date dataFimExpo;
    private Date dataInicioSubmissao;
    private Date dataFimSubmissao;
    private Date dataFimAlteracaoConflitos;
    private ListaOrganizadores lstOrganizadores;
    private ListaFae lstFae;
    private ListaDemonstracoes lstDemonstracoes;
    private ListaCandidaturas lstCandidaturas;
    private ListaAtribuicoes lstAtribuicoes;
    private ExposicaoState estado;
    private List<Tarefa> lstTarefas;

    public Exposicao() {
        local = new Local();
        lstOrganizadores = new ListaOrganizadores();
        lstDemonstracoes = new ListaDemonstracoes();
        lstCandidaturas = new ListaCandidaturas();
        lstAtribuicoes = new ListaAtribuicoes();
        lstFae = new ListaFae();
        estado = new ExposicaoInicialState(this);
        lstTarefas = new ArrayList<>();
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setTxtDescritivo(String txtDescritivo) {
        this.txtDescritivo = txtDescritivo;
    }

    public void setLocal(String local) {
        this.local.setLocal(local);
    }

    public void setDataInicioExpo(Date dataInicioExpo) {
        this.dataInicioExpo = dataInicioExpo;
    }

    public void setDataFimExpo(Date dataFimExpo) {
        this.dataFimExpo = dataFimExpo;
    }

    public void setDataInicioSubmissao(Date dataInicioSubmissao) {
        this.dataInicioSubmissao = dataInicioSubmissao;
    }

    public void setDataFimSubmissao(Date dataFimSubmissao) {
        this.dataFimSubmissao = dataFimSubmissao;
    }
    
    public void setDataFimAlteracaoConflitos(Date dataFimAlteracaoConflitos) {
        this.dataFimAlteracaoConflitos = dataFimAlteracaoConflitos;
    }
    
    public boolean setListaOrganizadores(List<Organizador> lstTmpOrganizadores){
        return getLstOrganizadores().adicionaOrganizadores(lstTmpOrganizadores);
    }
    
    public ListaOrganizadores novaListaOrganizadores() {
        return new ListaOrganizadores();
    }

    @Override
    public String toString() {
        String print = "";
        print += "Titulo: " + titulo;
        print += "\nTexto Descritivo: " + txtDescritivo;
        print += "\nLocal: " + local.getLocal();
        print += "\nData Inicio Exposição: " + dataInicioExpo;
        print += "\nData Fim Exposição: " + dataFimExpo;
        print += "\nData Inicio Submissões: " + dataInicioSubmissao;
        print += "\nData Fim Submissoões: " + dataFimSubmissao;
        print += "\nData Fim Alteração de Conflitos: " + dataFimAlteracaoConflitos;
        print += "\nOrganizadores da Exposição:";
        print += lstOrganizadores.toString();
        return print;
    }

    public void setEstado(ExposicaoState novoEstado) {
        estado = novoEstado;
    }

    public int getNrOrganizadores() {
        return getLstOrganizadores().getNrOrganizadores();
    }
    
    public boolean setExposicaoCriada(){
        return getEstado().setExposicaoCriada();
    }

    public Date getDataInicioSubmissao() {
        return dataInicioSubmissao;
    }

    public Date getDataFimSubmissao() {
        return dataFimSubmissao;
    }

    public Date getDataFimAlteracaoConflitos() {
        return dataFimAlteracaoConflitos;
    }
    
    @Override
    public void schedule(Tarefa tarefa, Date dataExecucao) {
        Timer temporizador = new Timer();
        temporizador.schedule(tarefa, dataExecucao);
        getLstTarefas().add(tarefa);
    }

    public void setExposicaoEmSubmissao() {
        getEstado().setExposicaoEmSubmissao();
    }

    public void setOrganizador(Utilizador u) {
        Organizador o = getLstOrganizadores().novoOrganizador(u);
        getLstOrganizadores().adicionaOrganizador(o);
    }

    public String getTitulo() {
        return titulo;
    }

    public String getTxtDescritivo() {
        return txtDescritivo;
    }
    public String getLocal() {
        return local.getLocal();
    }

    public Date getDataInicioExpo() {
        return dataInicioExpo;
    }

    public Date getDataFimExpo() {
        return dataFimExpo;
    }

    public ListaOrganizadores getLstOrganizadores() {
        return lstOrganizadores;
    }
    public ExposicaoState getEstado() {
        return estado;
    }

    public List<Tarefa> getLstTarefas() {
        return lstTarefas;
    }

    public ListaFae novaListaFae() {
        return new ListaFae();
    }

    public boolean adicionaListaFAE(ListaFae lstTmpFae) {
        return lstFae.adicionaListaFae(lstTmpFae);
    }

    public void setExposicaoComFae() {
        estado.setExposicaoComFae();
    }

    public ListaFae getLstFae() {
        return lstFae;
    }

    public ListaDemonstracoes getLstDemonstracoes() {
        return lstDemonstracoes;
    }

    void getInfo() {
        //metodo abstrato
    }

    public ListaCandidaturas getLstCandidaturas() {
        return lstCandidaturas;
    }

    public ListaAtribuicoes novaListaAtribuicoes() {
        return new ListaAtribuicoes();
    }

    public boolean setListaAtribuicoes(List<Atribuicao> lstAtribuicao) {
        return lstAtribuicoes.adiciona(lstAtribuicao);
    }

    @Override
    public void setCandidaturasAtribuidas() {
        for(int i=0;i<lstCandidaturas.getNrCandidaturas();i++){
            lstCandidaturas.get(i).setAtribuida();
        }
    }
    
    
}