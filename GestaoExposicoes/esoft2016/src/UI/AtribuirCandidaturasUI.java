package UI;

import Controller.AtribuirCandidaturasController;
import Util.Utils;
import java.util.List;


public class AtribuirCandidaturasUI {
        private String userName;
        private AtribuirCandidaturasController acController;
        private int mecanismo;
    
    public AtribuirCandidaturasUI(String userName){
        this.userName = userName;
        acController = new AtribuirCandidaturasController();
    }
        
    public void run(){
        apresentaExpoOrganizador();
        apresentaMecanismosDisponiveis();
        apresentaResultadoMecanismo();
    }
    public void apresentaExpoOrganizador() {
        List lstExposicoes = acController.getExposOrganizador(userName);
        int exposicao = Utils.apresentaListaSelecionaUm(lstExposicoes, "Lista de Exposições");
        acController.setExposicao(exposicao);
    }

    public void apresentaMecanismosDisponiveis() {
            List mecanismos = acController.getMecanismos();
            mecanismo = Utils.apresentaListaSelecionaUm(mecanismos, "Lista de Mecanismos de Atribuição");
    }

    public void apresentaResultadoMecanismo() {
        boolean confirma = false;
        do{
            List resultado = acController.setMecanismo(mecanismo);
            confirma = Utils.apresentaObjetoSolicitaConfirmacao(resultado, "Lista de atribuições");
        }while(!confirma);
        boolean sucesso = acController.guarda();
        notificaSucesso(sucesso);
    }
    
    public void notificaSucesso(boolean sucesso) {
        if(sucesso){
            System.out.println("Sucesso");
        }
        else{
            System.out.println("ERRO: Atribuicoes não guardadas");
        }
        Utils.readLineFromConsole("prima Enter para continuar...");
        Utils.limpaConsola();
    }

}