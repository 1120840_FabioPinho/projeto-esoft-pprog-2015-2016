package Estados;

import Dominio.Exposicao;
import java.util.Date;

public class ExposicaoCompletaState implements ExposicaoState{
    private Exposicao exposicao;

    public ExposicaoCompletaState(Exposicao exposicao) {
        this.exposicao = exposicao;
    }
    
    @Override
    public boolean setExposicaoCriada() {
        return false;
    }

    @Override
    public boolean setExposicaoComFaeSemDemonstracoes() {
        return false;
    }

    @Override
    public boolean setExposicaoComDemonstracoesSemFae() {
        return false;
    }

    @Override
    public boolean setExposicaoCompleta() {
        return true;
    }

    @Override
    public boolean setExposicaoEmSubmissao() {
        if(valida()){
            ExposicaoEmSubmissaoState eesState = new ExposicaoEmSubmissaoState(exposicao);
            exposicao.setEstado(eesState);
        }
        return false;
    }

    @Override
    public boolean setExposicaoForaSubmissao() {
        return false;
    }

    @Override
    public boolean setExposicaoComConflitosDetetados() {
        return false;
    }

    @Override
    public boolean setExposicaoComConflitosDetetadosAlterados() {
        return false;
    }

    @Override
    public boolean setExposicaoComCandidaturasAtribuidas() {
        return false;
    }

    @Override
    public boolean setExposicaoComTodasCandidaturasAvaliadas() {
        return false;
    }

    @Override
    public boolean valida() {
        Date dataAtual = new Date();
        if(dataAtual.equals(exposicao.getDataInicioSubmissao())){
            return true;
        }
        return false;
    }

    @Override
    public boolean setExposicaoComFae() {
        return false;
    }
    
}
