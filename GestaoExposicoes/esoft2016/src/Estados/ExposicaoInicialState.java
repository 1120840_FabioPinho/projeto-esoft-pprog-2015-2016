package Estados;

import Dominio.Exposicao;
import Tarefas.SetEstadoExposicaoConflitosAlterados;
import Tarefas.SetEstadoExposicaoEmSubmissao;
import Tarefas.SetEstadoExposicaoFimSubmissao;
import Tarefas.Tarefa;
import Tarefas.UC13_DetetarConflitosDeInteresse;
import java.util.Date;

public class ExposicaoInicialState implements ExposicaoState{
    private Exposicao exposicao;
    public ExposicaoInicialState(Exposicao expo) {
        this.exposicao = expo;
    }

    @Override
    public boolean setExposicaoCriada() {
        if(valida()){
            Tarefa setEstadoExposicaoEmSubmissao = new SetEstadoExposicaoEmSubmissao(exposicao);
            Date dtInicioSubmissao = exposicao.getDataInicioSubmissao();
            exposicao.schedule(setEstadoExposicaoEmSubmissao, dtInicioSubmissao);
            
            Tarefa setEstadoExposicaoFimSubmissao = new SetEstadoExposicaoFimSubmissao(exposicao);
            Date dtFimSubmissao = exposicao.getDataFimSubmissao();
            exposicao.schedule(setEstadoExposicaoFimSubmissao, dtFimSubmissao);
            
            Tarefa uc13_DetetarConflitosDeInteresse = new UC13_DetetarConflitosDeInteresse(exposicao);
            exposicao.schedule(uc13_DetetarConflitosDeInteresse, dtFimSubmissao);
            
            Tarefa setEstadoExposicaoConflitosAlterados = new SetEstadoExposicaoConflitosAlterados(exposicao);
            Date dtFimAlteracaoConflitos = exposicao.getDataFimAlteracaoConflitos();
            exposicao.schedule(setEstadoExposicaoConflitosAlterados,dtFimAlteracaoConflitos);
            
            ExposicaoState ecState = new ExposicaoCriadaState(exposicao);
            exposicao.setEstado(ecState);
            return true;
        }
        return false;
    }

    @Override
    public boolean valida() {
        int nrOrganizadores = exposicao.getNrOrganizadores();
        return nrOrganizadores >= 2;
    }

    @Override
    public boolean setExposicaoComFaeSemDemonstracoes() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoComDemonstracoesSemFae() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoCompleta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoEmSubmissao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoForaSubmissao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoComConflitosDetetados() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoComConflitosDetetadosAlterados() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoComCandidaturasAtribuidas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoComTodasCandidaturasAvaliadas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoComFae() {
        return false;
    }
    
}
