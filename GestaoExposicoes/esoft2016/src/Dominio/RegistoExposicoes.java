package Dominio;

import java.util.ArrayList;
import java.util.List;

public class RegistoExposicoes {
    private List<Exposicao> lstExposicoes;

    public RegistoExposicoes() {
        lstExposicoes = new ArrayList<>();
    }
    
    List<Exposicao> getListaExposicoes() {
        return lstExposicoes;
    }

    public Exposicao novaExposicao() {
        return new Exposicao();
    }
    
    public boolean validaExposicao(Exposicao expo){
        //todo validação de expo repetida
        return true;
    }
    
    public boolean adicionaExposicao (Exposicao expo){
        return lstExposicoes.add(expo);
    }

    public List<Exposicao> getExposOrganizador(String userName) {
        List<Exposicao> lstExposOrganizador = new ArrayList<>();
        for (Exposicao exposicao : lstExposicoes) {
            ListaOrganizadores lstOrganizadores = exposicao.getLstOrganizadores();
            try{
                Organizador o = lstOrganizadores.getOrganizador(userName);
                lstExposOrganizador.add(exposicao);
            }
            catch(NullPointerException ex){
                
            }
        }
        return lstExposOrganizador;
    }
    
}
