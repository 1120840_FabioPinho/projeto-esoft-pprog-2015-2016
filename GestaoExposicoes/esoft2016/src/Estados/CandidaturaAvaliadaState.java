package Estados;

import Dominio.Candidatura;

public class CandidaturaAvaliadaState implements CandidaturaState{
    
    private Candidatura candidatura;

    public CandidaturaAvaliadaState(Candidatura candidatura) {
        this.candidatura = candidatura;
    }
    
    
    @Override
    public boolean setAtribuida() {
        return false;
    }

    @Override
    public boolean setCandidaturaComConflitosDetetados() {
        return false;
    }

    @Override
    public boolean valida() {
        return false;
    }

    @Override
    public boolean setCandidaturaAvaliada() {
        return false;
    }

    @Override
    public boolean setCandidaturaSubmetida() {
        return false;
    }

    @Override
    public boolean setEmSubmissao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setSubmissaoEncerrada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setEmAvaliacao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setNaoAvaliada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setRejeitada() {
        if (valida()){
            CandidaturaRejeitadaState crState = new CandidaturaRejeitadaState(candidatura);
            candidatura.setEstado(crState);
            return true;
        }
        return false;
    }

    @Override
    public boolean setAceite() {
        if (valida()){
            CandidaturaAceiteState caState = new CandidaturaAceiteState(candidatura);
            candidatura.setEstado(caState);
            return true;
        }
        return false;
    }

    @Override
    public boolean setRetirada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setStandAtribuido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setStandEfetivo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
