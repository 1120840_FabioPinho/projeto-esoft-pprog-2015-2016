package Controller;

import Dominio.CentroDeExposicoes;
import Dominio.RegistoUtilizadores;
import Dominio.Utilizador;

public class LoginController {
    private Utilizador uLoggedIn;
    private RegistoUtilizadores registoUtilizadores;
    private CentroDeExposicoes centro;
    public LoginController() {
        centro = CentroDeExposicoes.getInstancia();
        registoUtilizadores = centro.getRegistoUtilizadores();
    }

    public void fazLogin(String username, String password) {
        uLoggedIn = registoUtilizadores.getUtilizador(username, password);
    }

    public boolean utilizadorLoggedIn() {
        return uLoggedIn != null;
    }
}
