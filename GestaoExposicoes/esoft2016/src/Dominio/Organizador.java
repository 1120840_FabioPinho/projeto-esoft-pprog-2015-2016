package Dominio;

public class Organizador {
    private Utilizador utilizador;
    
    public Organizador() {
        utilizador = new Utilizador();
    }

    public Organizador(Utilizador utilizador) {
        this.utilizador = new Utilizador();
        this.utilizador.setNome(utilizador.getNome());
        this.utilizador.setEmail(utilizador.getEmail());
        this.utilizador.setUsername(utilizador.getUsername());
        this.utilizador.setPassword(utilizador.getPassword());
    }
    
    public boolean valida(){
        return false;
    }
    
    public String getUsername(){
        return utilizador.getUsername();
    }

    @Override
    public String toString() {
        return utilizador.getNome();
    }
 
}
