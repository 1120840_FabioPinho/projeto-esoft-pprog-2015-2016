package Controller;

import Dominio.CentroDeExposicoes;
import Dominio.Exposicao;
import Dominio.FAE;
import Dominio.ListaFae;
import Dominio.Organizador;
import Dominio.RegistoExposicoes;
import Dominio.RegistoUtilizadores;
import Dominio.Utilizador;
import java.util.ArrayList;
import java.util.List;

public class DefinirFaeController {
    private Organizador organizador;
    private CentroDeExposicoes centro;
    private List<Exposicao> lstExpoOrganizador;
    private Exposicao exposicao;
    private RegistoUtilizadores lstUtilizadores;
    private ListaFae lstTmpFae;
    
    public DefinirFaeController() {
        centro = CentroDeExposicoes.getInstancia();
    }
    
    public List getExposOrganizador(String userName){
        List<String> retorno = new ArrayList<>();
        RegistoExposicoes lstExposicoes = centro.getRegistoExposicoes();
        lstExpoOrganizador = lstExposicoes.getExposOrganizador(userName);
        for (Exposicao expoOrganizador : lstExpoOrganizador) {
            retorno.add(expoOrganizador.getTitulo());
        }
        return retorno;
    }

    public void setExposicao(int exposicao) {
        this.exposicao = lstExpoOrganizador.get(exposicao);
    }
    
    public List obterUtilizadores() {
        List<String> retorno = new ArrayList<>();
        lstUtilizadores = centro.getRegistoUtilizadores();
        List<Utilizador> listaUtilizadores = lstUtilizadores.getTodosUtilizadores();
        for (Utilizador utilizador : listaUtilizadores) {
            retorno.add(utilizador.getUsername());
        }
        lstTmpFae = exposicao.novaListaFae();
        return retorno;
    }

    public boolean adicionaFae(int u) {
        Utilizador utilizador = lstUtilizadores.get(u);
        FAE fae = lstTmpFae.novoFae(utilizador);
        boolean valido = lstTmpFae.validaFae(fae);
        if(valido){
            boolean adicionado = lstTmpFae.adicionaFae(fae);
            return adicionado;
        }
        return false;
    }

    public Object getFaeIntroduzidos() {
        return lstTmpFae;
    }

    public boolean registaFae() {
        exposicao.setExposicaoComFae();
        return exposicao.adicionaListaFAE(lstTmpFae);
    }
}
