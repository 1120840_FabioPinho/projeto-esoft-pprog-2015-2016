package Dominio;

import java.util.ArrayList;
import java.util.List;

public class ListaAtribuicoes {
    private List<Atribuicao> lstAtribuicoes;

    public ListaAtribuicoes() {
        lstAtribuicoes = new ArrayList<>();
    }
    
    public Atribuicao novaAtribuicao(){
        return new Atribuicao();
    }

    public boolean adiciona(Atribuicao atribuicao) {
        return lstAtribuicoes.add(atribuicao);
    }
    
    public boolean adiciona(List<Atribuicao> atribuicoes) {
        for (Atribuicao atribuicao : atribuicoes) {
            lstAtribuicoes.add(atribuicao);
        }
        return true;
    }

    public List<Atribuicao> getListaAtribuicoes() {
        return lstAtribuicoes;
    }
}
