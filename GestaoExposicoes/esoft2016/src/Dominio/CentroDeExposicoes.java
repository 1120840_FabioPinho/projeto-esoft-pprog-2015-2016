package Dominio;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CentroDeExposicoes {
    static CentroDeExposicoes instancia;
    private RegistoUtilizadores registoUtilizadores;
    private RegistoExposicoes registoExposicoes;
    private List<MecanismoAtribuicao> lstMecanismosAtribuicao;
    
    private CentroDeExposicoes() {
        registoUtilizadores = new RegistoUtilizadores();
        registoExposicoes = new RegistoExposicoes();
        carregaMecanismos();
    }
    
    public static CentroDeExposicoes getInstancia(){
        if(instancia == null){
            instancia = new CentroDeExposicoes();
        }
        return instancia;
    }
    
    public RegistoUtilizadores getRegistoUtilizadores() {
        return registoUtilizadores;
    }

    public RegistoExposicoes getRegistoExposicoes() {
        return registoExposicoes;
    }

    public List getMecanismosDeAtribuicao() {
        return lstMecanismosAtribuicao;
    }

    private void carregaMecanismos() {
        Scanner input;
        lstMecanismosAtribuicao  = new ArrayList<>();
        try {
            String basePath = new File("").getAbsolutePath();
            input = new Scanner(new File(basePath+"\\src\\dadosParaTestes\\mecanismosAtribuicao.txt"));
            input.useDelimiter(";|\n");
            while(input.hasNext()) {
                try {
                    String nomeClasse = input.next();
                    MecanismoAtribuicao mecanismo = (MecanismoAtribuicao) Class.forName(nomeClasse).newInstance();
                    lstMecanismosAtribuicao.add(mecanismo);
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                    System.out.println("falhou a forma automática");
                }
                MecanismoAtribuicao mecanismoA = new MecanismoAtribuicaoA();
                MecanismoAtribuicao mecanismoB = new MecanismoAtribuicaoB();
                lstMecanismosAtribuicao.add(mecanismoA);
                lstMecanismosAtribuicao.add(mecanismoB);
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Ficheiro Não Encontrado!!");
        }
    }
}
