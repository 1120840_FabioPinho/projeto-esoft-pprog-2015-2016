package UI;

import Controller.DefinirFaeController;
import Util.Utils;
import java.util.List;

public class DefinirFaeUI implements UI{
    private String userName;
    private DefinirFaeController dfaeController;
    
    public DefinirFaeUI(String userName){
        this.userName = userName;
        dfaeController = new DefinirFaeController();
    }
    
    public void run(){
        apresentaExposicoesOrganizador();
        apresentaUtilizadores();
        apresentaFaeIntroduzidos();
    }

    private void apresentaExposicoesOrganizador() {
        Utils.limpaConsola();
        List expos = dfaeController.getExposOrganizador(userName);
        int exposicao = Utils.apresentaListaSelecionaUm(expos,"Lista de Exposicoes do Organizador");
        dfaeController.setExposicao(exposicao);
    }

    private void apresentaUtilizadores() {
        boolean adicionarMais;
        do{
            Utils.limpaConsola();
            int utilizador= Utils.apresentaListaSelecionaUm(dfaeController.obterUtilizadores(),"Lista de Utilizadores");
            boolean faeAdicionado = dfaeController.adicionaFae(utilizador);
            if(faeAdicionado){
                System.out.println("Fae adicionado com sucesso!!");
            }
            else{
                System.out.println("Erro: algo falhou ao adicionar Fae");
            }
            adicionarMais = Utils.confirma("Deseja introduzir mais Fae??");
        }while(adicionarMais);
    }

    @Override
    public void notificaSucesso(boolean sucesso) {
        Utils.limpaConsola();
        if(sucesso){
            System.out.println("Sucesso");
        }
        else{
            System.out.println("ERRO: Lista de Fae não adicionada ");
        }
        Utils.readLineFromConsole("prima Enter para continuar...");
        Utils.limpaConsola();
    }

    private void apresentaFaeIntroduzidos() {
        Utils.limpaConsola();
        boolean confirma = Utils.apresentaObjetoSolicitaConfirmacao(dfaeController.getFaeIntroduzidos(), "Lista de Fae Introduzidos");
        if(confirma){
            boolean sucesso = dfaeController.registaFae();
            notificaSucesso(sucesso);
        }
        else{
            System.out.println("Operação cancelada");
        }
    }
    
}
