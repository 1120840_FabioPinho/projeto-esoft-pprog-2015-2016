package Dominio;

import Estados.CandidaturaEmAvaliacao;
import Estados.CandidaturaState;

public class CandidaturaDemonstracao implements CandidaturaAtribuivel, CandidaturaAvaliavel {
    
    private String nomeEmpresa;
    private String morada;
    private int telemovel;
    private float areaExposicao;
    private int nrConvites;
    private ListaProdutos lstProdutos;
    private CandidaturaState estado;
    
    public CandidaturaDemonstracao(){
    }
    
    public void getListaAvaliacoes() {
            // TODO - implement Candidatura.getListaAvaliacoes
            throw new UnsupportedOperationException();
    }

    public void novoProduto() {
            // TODO - implement Candidatura.novoProduto
            throw new UnsupportedOperationException();
    }

    public void setNomeEmpresa(String nomeEmpresa) {
            this.nomeEmpresa = nomeEmpresa;
    }

    public void setMorada(String morada) {
            this.morada = morada;
    }

    public void setTelemovel(int telemovel) {
            this.telemovel = telemovel;
    }

    public void setAreaExposicao(float areaExposicao) {
            this.areaExposicao = areaExposicao;
    }
    
    public void setNrConvites(int nrConvites) {
            this.nrConvites = nrConvites;
    }

    @Override
    public void setAtribuida() {
        estado.setAtribuida();
    }

    public void setEstado(CandidaturaState novoEstado) {
        estado = novoEstado;
    }

    @Override
    public void setCandidaturaAvaliada() {
        estado.setCandidaturaAvaliada();
    }
    
}
