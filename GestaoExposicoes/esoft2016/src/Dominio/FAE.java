package Dominio;

public class FAE {
    private Utilizador utilizador;
    
    public FAE(Utilizador utilizador) {
        this.utilizador = new Utilizador();
        this.utilizador.setNome(utilizador.getNome());
        this.utilizador.setEmail(utilizador.getEmail());
        this.utilizador.setUsername(utilizador.getUsername());
        this.utilizador.setPassword(utilizador.getPassword());
    }
    
    public String getUserName(){
        return utilizador.getUsername();
    }
}
