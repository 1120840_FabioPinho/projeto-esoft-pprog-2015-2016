package Controller;

import Dominio.CentroDeExposicoes;
import Dominio.Exposicao;
import Dominio.MecanismoAtribuicao;
import Dominio.RegistoExposicoes;
import Dominio.Atribuicao;
import Dominio.ListaAtribuicoes;
import java.util.ArrayList;
import java.util.List;

public class AtribuirCandidaturasController {
    CentroDeExposicoes centro;
    List<Exposicao> lstExpoOrganizador;
    List<Atribuicao> lstAtribuicao;
    Exposicao exposicao;
    List<MecanismoAtribuicao> lstMecanismos;
    
    public AtribuirCandidaturasController() {
        centro = CentroDeExposicoes.getInstancia();
        lstAtribuicao = new ArrayList<Atribuicao>();
    }
	
    public List getExposOrganizador(String userName){
        List<String> retorno = new ArrayList<>();
        RegistoExposicoes lstExposicoes = centro.getRegistoExposicoes();
        lstExpoOrganizador = lstExposicoes.getExposOrganizador(userName);
        for (Exposicao expoOrganizador : lstExpoOrganizador) {
            retorno.add(expoOrganizador.getTitulo());
        }
        return retorno;
    }
    
    public void setExposicao(int exposicao) {
        this.exposicao = lstExpoOrganizador.get(exposicao);
    }

    public List getMecanismos() {
        lstMecanismos = centro.getMecanismosDeAtribuicao();
        return lstMecanismos;
    }

    public List setMecanismo(int mecanismo) {
        MecanismoAtribuicao mAtribuicao = lstMecanismos.get(mecanismo);
        ListaAtribuicoes lstTmp = mAtribuicao.atribui(exposicao);
        lstAtribuicao = lstTmp.getListaAtribuicoes();
        return lstAtribuicao;
    }
    public boolean guarda() {
        exposicao.setCandidaturasAtribuidas();
        return exposicao.setListaAtribuicoes(lstAtribuicao);
    }

}