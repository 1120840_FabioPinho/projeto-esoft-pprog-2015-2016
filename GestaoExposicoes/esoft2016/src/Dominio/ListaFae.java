package Dominio;
import java.util.ArrayList;
import java.util.List;

public class ListaFae {
    private List<FAE> lstFae;

    public ListaFae() {
        lstFae = new ArrayList<>();
    }

    @Override
    public String toString() {
        String print = "";
        for (FAE fae : lstFae) {
            print += "\n" + fae.getUserName();
        }
        return print;
    }

    public FAE novoFae(Utilizador utilizador) {
        return new FAE(utilizador);
    }

    public boolean validaFae(FAE fae) {
        //todo validar se existem repetidos
        return true;
    }

    public boolean adicionaFae(FAE fae) {
        return lstFae.add(fae);
    }

    boolean adicionaListaFae(ListaFae lstTmpFae) {
        boolean sucesso = false;
        List<FAE> listaTmpFae = lstTmpFae.getListaFae();
        for (FAE fae : listaTmpFae) {
            sucesso = lstFae.add(fae);
        }
        return sucesso;
    }

    public List<FAE> getListaFae() {
        return lstFae;
    }

    public int getNrFae() {
        return lstFae.size();
    }

    
}
