package Controller;

import Dominio.Exposicao;
import Dominio.Utilizador;
import PersistentData.CarregarExposicoes;
import PersistentData.CarregarUtilizadores;
import PersistentData.ExposicoesFicheiro;
import PersistentData.UtilizadoresFicheiro;
import java.util.Date;
import java.util.List;

public class InicializaDadosAplicacaoController {
    private CarregarUtilizadores repositorioUtilizadores;
    private CarregarExposicoes repositorioExposicoes;
    private CriarPerfilDeUtilizadorController cpuController;
    public InicializaDadosAplicacaoController() {
       repositorioUtilizadores = new UtilizadoresFicheiro();
       repositorioExposicoes = new ExposicoesFicheiro();
    }
    public void carregaUtilizadores(){
        List<Utilizador> lstUtilizadores;
        lstUtilizadores = repositorioUtilizadores.carregaUtilizadores();
        for (Utilizador utilizador : lstUtilizadores) {
            cpuController = new CriarPerfilDeUtilizadorController();
            String nome = utilizador.getNome();
            String email = utilizador.getEmail();
            String password = utilizador.getPassword();
            String username = utilizador.getUsername();
            cpuController.defineUtilizador(nome, email, username, password);
            cpuController.registaUtilizador();
        }
    }
    public void carregaExposicoes(){
        List<Exposicao> lstExposicoes = repositorioExposicoes.carregaExposicoes();
        CriarExposicaoController ceController = new CriarExposicaoController();
        for (Exposicao expo : lstExposicoes) {
            String titulo = expo.getTitulo();
            String txtDescritivo = expo.getTxtDescritivo();
            String local = expo.getLocal();
            Date dataInicioExpo = expo.getDataInicioExpo();
            Date dataFimExpo = expo.getDataFimExpo();
            Date dataInicioSubmissao = expo.getDataInicioSubmissao();
            Date dataFimSubmissao = expo.getDataFimSubmissao();
            Date dataFimAlteracaoConflitos =  expo.getDataFimAlteracaoConflitos();
            ceController.setDadosExpo(titulo, txtDescritivo, local, dataInicioExpo, dataFimExpo, dataInicioSubmissao, dataFimSubmissao, dataFimAlteracaoConflitos);
            ceController.setOrganizadores(expo.getLstOrganizadores());
            ceController.registaExposicao();
        }
    }
    public void carregaCandidaturas(){}
}
