package Dominio;

import java.util.ArrayList;
import java.util.List;

public class ListaOrganizadores {
    private List<Organizador> lstOrganizadores;

    public ListaOrganizadores() {
        lstOrganizadores = new ArrayList<>();
    }

    public List<Organizador> getTodosOrganizadores() {
        return lstOrganizadores;
    }
    
    public Organizador novoOrganizador(){
        return new Organizador();
    }
    
    public boolean validaOrganizador(Organizador organizador){
        for (Organizador organizadorMembro : lstOrganizadores) {
            if(organizador.getUsername().equals(organizadorMembro.getUsername())){
                return false;
            }
        }
        return true;
    }
    
    public boolean adicionaOrganizador(Organizador organizador){
        return lstOrganizadores.add(organizador);
    }

    public boolean adicionaOrganizadores(List<Organizador> lstTmpOrganizadores) {
        boolean retorno = false;
        for (Organizador organizador : lstTmpOrganizadores) {
            retorno = lstOrganizadores.add(organizador);
        }
        return retorno;
    }

    public Organizador novoOrganizador(Utilizador utilizador) {
        return new Organizador(utilizador);
    }

    @Override
    public String toString() {
        String print = "";
        for(Organizador organizador : lstOrganizadores){
            print += "\n" + organizador.toString();
        }
        return print;
    }

    public int getNrOrganizadores() {
        return lstOrganizadores.size();
    }

    public Organizador getOrganizador(String userName) {
        for (Organizador organizador : lstOrganizadores) {
            if(organizador.getUsername().equals(userName)){
                return organizador;
            }
        }
        return null;
    }
    
    
}
