package Dominio;

import java.util.ArrayList;
import java.util.List;

public class RegistoUtilizadores {
    private List<Utilizador> lstUtilizadores;

    public RegistoUtilizadores() {
        lstUtilizadores = new ArrayList<>();
    }
    
    public Utilizador getUtilizador(String username, String password){
        for (Utilizador utilizador : lstUtilizadores) {
            if(username.equals(utilizador.getUsername()) && password.equals(utilizador.getPassword())){
                return utilizador;
            }
        }
    return null;
    }

    public List<Utilizador> getListaUtilizadores() {
        return lstUtilizadores;
    }

    public List<Utilizador> getTodosUtilizadores() {
        return lstUtilizadores;
    }

    public Utilizador novoUtilizador() {
        return new Utilizador();
    }

    public boolean adicionaUtilizador(Utilizador utilizador) {
        if(utilizador.valida()){
            return lstUtilizadores.add(utilizador);
        }
        return false;
    }

    public boolean validaUtilizador(String username, String email) {
        for (Utilizador utilizador : lstUtilizadores){
            String utilizadorUsername = utilizador.getUsername();
            String utilizadorEmail = utilizador.getEmail();
            if(utilizadorUsername.equalsIgnoreCase(username)||utilizadorEmail.equalsIgnoreCase(email)){
                return true;
            }
        }
        return false;
    }

    public Utilizador get(int index) {
        return lstUtilizadores.get(index);
    }

    public Utilizador getUtilizador(String username) {
        for (Utilizador utilizador : lstUtilizadores) {
            if(utilizador.getUsername().equals(username)){
                return utilizador;
            }
        }
        return null;
    }
}
