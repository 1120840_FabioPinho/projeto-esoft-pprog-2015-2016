package Controller;

import Dominio.CentroDeExposicoes;
import Dominio.RegistoUtilizadores;
import Dominio.Utilizador;

public class CriarPerfilDeUtilizadorController {
    private CentroDeExposicoes centro;
    private RegistoUtilizadores registoUtilizadores;
    private Utilizador utilizador;
    
    public CriarPerfilDeUtilizadorController() {
        centro = CentroDeExposicoes.getInstancia();
        registoUtilizadores = centro.getRegistoUtilizadores();
        utilizador = registoUtilizadores.novoUtilizador();
    }
    

    public boolean defineUtilizador(String nome, String email, String username, String password) {
        boolean existente = registoUtilizadores.validaUtilizador(username,email);
        if(!existente){
            utilizador.setNome(nome);
            utilizador.setEmail(email);
            utilizador.setUsername(username);
            utilizador.setPassword(password);
            return true;
        }
        return false;
    }

    public String dadosUtilizadorIntroduzidos() {
        return utilizador.toString();
    }

    public boolean registaUtilizador() {
        return registoUtilizadores.adicionaUtilizador(utilizador);
    }
    
}
