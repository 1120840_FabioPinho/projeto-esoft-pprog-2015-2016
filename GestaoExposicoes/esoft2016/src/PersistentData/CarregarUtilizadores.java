package PersistentData;

import Dominio.Utilizador;
import java.util.List;

public interface CarregarUtilizadores {
    public List<Utilizador> carregaUtilizadores();
}
