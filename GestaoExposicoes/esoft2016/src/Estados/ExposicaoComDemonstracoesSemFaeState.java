package Estados;

import Dominio.Exposicao;
import Dominio.ListaFae;

public class ExposicaoComDemonstracoesSemFaeState implements ExposicaoState{
    private Exposicao exposicao;

    public ExposicaoComDemonstracoesSemFaeState(Exposicao exposicao) {
        this.exposicao = exposicao;
    }
    
    @Override
    public boolean setExposicaoCriada() {
        return false;
    }

    @Override
    public boolean setExposicaoEmSubmissao() {
        return false;
    }

    @Override
    public boolean setExposicaoComFaeSemDemonstracoes() {
        return false;
    }

    @Override
    public boolean setExposicaoComDemonstracoesSemFae() {
        return true;
    }

    @Override
    public boolean setExposicaoForaSubmissao() {
        return false;
    }

    @Override
    public boolean setExposicaoComConflitosDetetados() {
        return false;
    }

    @Override
    public boolean setExposicaoComConflitosDetetadosAlterados() {
        return false;
    }

    @Override
    public boolean setExposicaoComCandidaturasAtribuidas() {
        return false;
    }

    @Override
    public boolean setExposicaoComTodasCandidaturasAvaliadas() {
        return false;
    }

    @Override
    public boolean setExposicaoComFae() {
        return setExposicaoCompleta();
    }
    
    @Override
    public boolean setExposicaoCompleta() {
        if(valida()){
            ExposicaoCompletaState ecState = new ExposicaoCompletaState(exposicao);
            exposicao.setEstado(ecState);
            return true;
        }
        return false;
    }
    
    @Override
    public boolean valida() {
        ListaFae lstFae = exposicao.getLstFae();
        if(lstFae.getNrFae()==0){
            return true;
        }
        return false;
    }
    
}
