package Estados;

import Dominio.Candidatura;
import Dominio.Demonstracao;

public class CandidaturaEmAvaliacao implements CandidaturaState{

    private Candidatura candidatura;
    
    @Override
    public boolean setAtribuida() {
        return true;
    }

    @Override
    public boolean setCandidaturaComConflitosDetetados() {
        return false;
    }

    @Override
    public boolean valida() {
        return false;
    }

    @Override
    public boolean setCandidaturaAvaliada() {
        if (valida()){
            CandidaturaAvaliadaState caState = new CandidaturaAvaliadaState(candidatura);
            candidatura.setEstado(caState);
            return true;
        }
        return false;
    }

    @Override
    public boolean setCandidaturaSubmetida() {
        return false;
    }

    @Override
    public boolean setEmSubmissao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setSubmissaoEncerrada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setEmAvaliacao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setNaoAvaliada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setRejeitada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setAceite() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setRetirada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setStandAtribuido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setStandEfetivo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
