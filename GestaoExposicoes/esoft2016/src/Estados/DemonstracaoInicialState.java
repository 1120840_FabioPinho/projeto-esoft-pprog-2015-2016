package Estados;

import Dominio.Demonstracao;

public class DemonstracaoInicialState implements DemonstracaoState{
    private Demonstracao demonstracao;

    public DemonstracaoInicialState(Demonstracao demonstracao) {
        this.demonstracao = demonstracao;
    }

    @Override
    public boolean setCandidaturasAtribuidas() {
        return false;
    }

    @Override
    public boolean valida() {
        return true;
    }
    
    
}
