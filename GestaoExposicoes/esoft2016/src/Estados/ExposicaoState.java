package Estados;

public interface ExposicaoState {
    public boolean setExposicaoComFae();
    public boolean setExposicaoCriada();
    public boolean setExposicaoComFaeSemDemonstracoes();
    public boolean setExposicaoComDemonstracoesSemFae();
    public boolean setExposicaoCompleta();
    public boolean setExposicaoEmSubmissao();
    public boolean setExposicaoForaSubmissao();
    public boolean setExposicaoComConflitosDetetados();
    public boolean setExposicaoComConflitosDetetadosAlterados();
    public boolean setExposicaoComCandidaturasAtribuidas();
    public boolean setExposicaoComTodasCandidaturasAvaliadas();
    
    public boolean valida();
}
