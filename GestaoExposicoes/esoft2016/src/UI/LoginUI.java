package UI;

import Controller.LoginController;
import Util.Utils;

class LoginUI {
    private LoginController lcontroller;
    private String username;
    private String password;
    
    public LoginUI() {
        lcontroller = new LoginController();
    }
    
    public void run(){
        solicitaCredenciaisLogin();
        try{
            fazLogin();
        }
        catch(NullPointerException ex){
            if(utilizadorQuerSair()){
                Utils.sair();
            }
            else{
                System.out.println("Credenciais introduzidas inválidas!!!");
                Utils.readLineFromConsole("prima Enter para continuar...");
                Utils.limpaConsola();
                this.run();
            }
        }
    }

    private void solicitaCredenciaisLogin() {
        username = Utils.readLineFromConsole("Introduza nome de Utilizador: ");
        password = Utils.readLineFromConsole("Introduza palavra-chave: ");
    }

    private void fazLogin() {
        lcontroller.fazLogin(username,password);
    }

    boolean utilizadorQuerSair() {
        return username.equalsIgnoreCase("sair");
    }

    boolean loginTeveSucesso() {
        return lcontroller.utilizadorLoggedIn();
    }

    public String getUsername() {
        return username;
    }
}
