package Dominio;

import java.util.List;
import java.util.Random;

public class MecanismoAtribuicaoA implements MecanismoAtribuicao{
private Random geradorNrAleatorios;
    
    public MecanismoAtribuicaoA() {
        geradorNrAleatorios = new Random();
    }
    
    @Override
    public ListaAtribuicoes atribui(Exposicao exposicao) {
        exposicao.getInfo();
        ListaFae listaFae = exposicao.getLstFae();
        ListaCandidaturas listaCandidaturas = exposicao.getLstCandidaturas();
        List<FAE> lstFae = listaFae.getListaFae();
        List<CandidaturaExposicao> lstCandidaturas = listaCandidaturas.getTodasAsCandidaturas();
        ListaAtribuicoes lstAtribuicoes = exposicao.novaListaAtribuicoes();
        for (CandidaturaExposicao candidatura : lstCandidaturas) {
            Atribuicao atribuicao = lstAtribuicoes.novaAtribuicao();
                int nrFaes = geradorNrAleatorios.nextInt(listaFae.getNrFae());
                for(int i=0; i<nrFaes; i++){
                    FAE fae = lstFae.get(geradorNrAleatorios.nextInt(listaFae.getNrFae()));
                    atribuicao.setFae(fae);
                    atribuicao.setCandidatura(candidatura);
                }
            lstAtribuicoes.adiciona(atribuicao);
        }
        return lstAtribuicoes;
    }
    
    @Override
    public String toString() {
        return "Mecanismo A";
    }  
}
