package Dominio;

import java.util.ArrayList;
import java.util.List;

class ListaCandidaturas {
    private List<CandidaturaExposicao> lstCandidaturas;
    
    public ListaCandidaturas() {
        lstCandidaturas = new ArrayList<>();
    }

    int getNrCandidaturas() {
        return lstCandidaturas.size();
    }

    public List getTodasAsCandidaturas() {
        return lstCandidaturas;
    }

    public CandidaturaExposicao get(int index) {
        return lstCandidaturas.get(index);
    }
}
