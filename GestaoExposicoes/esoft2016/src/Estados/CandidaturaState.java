package Estados;

public interface CandidaturaState {
    public boolean setAtribuida();
    public boolean setCandidaturaComConflitosDetetados();
    public boolean valida();
    public boolean setCandidaturaAvaliada();
    public boolean setCandidaturaSubmetida();
    
    public boolean setEmSubmissao();
    public boolean setSubmissaoEncerrada();
    public boolean setEmAvaliacao();
    //public boolean setAvaliada();
    public boolean setNaoAvaliada();
    public boolean setRejeitada();
    public boolean setAceite();
    public boolean setRetirada();
    public boolean setStandAtribuido();
    public boolean setStandEfetivo();
}
