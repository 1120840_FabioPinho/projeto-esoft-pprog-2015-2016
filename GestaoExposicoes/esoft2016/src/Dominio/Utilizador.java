package Dominio;

public class Utilizador {
    private String nome;
    private String username;
    private String password;
    private String email;
    public Utilizador() {
    }

    public Utilizador(String nome, String email, String username, String password) {
        this.setNome(nome);
        this.setEmail(email);
        this.setUsername(username);
        this.setPassword(password);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Dados Utilizador\n" + "Nome:" + nome + "\nEmail:" + email + "\nUsername:" + username + "\nPassword:" + password + "\n";
    }

    boolean valida() {
        return true;
    }

}
