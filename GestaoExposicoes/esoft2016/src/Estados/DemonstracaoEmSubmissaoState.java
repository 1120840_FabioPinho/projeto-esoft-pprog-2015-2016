package Estados;

import Dominio.Demonstracao;

public class DemonstracaoEmSubmissaoState implements DemonstracaoState{
    private Demonstracao demonstracao;
    
    public DemonstracaoEmSubmissaoState(Demonstracao demonstracao) {
        this.demonstracao = demonstracao;
    }

    @Override
    public boolean setCandidaturasAtribuidas() {
        return false;
    }

    @Override
    public boolean valida() {
        return true;
    }
}
