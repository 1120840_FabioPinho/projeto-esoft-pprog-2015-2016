
package Estados;

import Dominio.Candidatura;

public class CandidaturaStandEfetivoState implements CandidaturaState{
    
    private Candidatura candidatura;

    public CandidaturaStandEfetivoState(Candidatura candidatura) {
        this.candidatura = candidatura;
    }

    @Override
    public boolean setAtribuida() {
        return false;
    }

    @Override
    public boolean setCandidaturaComConflitosDetetados() {
        return false;
    }

    @Override
    public boolean valida() {
        return false;
    }

    @Override
    public boolean setCandidaturaAvaliada() {
        return false;
    }

    @Override
    public boolean setCandidaturaSubmetida() {
        return false;
    }

    @Override
    public boolean setEmSubmissao() {
        return false;
    }

    @Override
    public boolean setSubmissaoEncerrada() {
        return false;
    }

    @Override
    public boolean setEmAvaliacao() {
        return false;
    }

    @Override
    public boolean setNaoAvaliada() {
        return false;
    }

    @Override
    public boolean setRejeitada() {
        return false;
    }

    @Override
    public boolean setAceite() {
        return false;
    }

    @Override
    public boolean setRetirada() {
        return false;
    }

    @Override
    public boolean setStandAtribuido() {
        return false;
    }

    @Override
    public boolean setStandEfetivo() {
        return true;
    }
    
    
}
