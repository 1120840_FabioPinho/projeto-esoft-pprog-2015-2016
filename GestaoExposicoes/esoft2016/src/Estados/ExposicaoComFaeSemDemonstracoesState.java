package Estados;

import Dominio.Exposicao;

public class ExposicaoComFaeSemDemonstracoesState implements ExposicaoState{
    private Exposicao exposicao;

    public ExposicaoComFaeSemDemonstracoesState(Exposicao exposicao) {
        this.exposicao = exposicao;
    }

    @Override
    public boolean setExposicaoComFaeSemDemonstracoes() {
        return true;
    }

    @Override
    public boolean setExposicaoComDemonstracoesSemFae() {
        return false;
    }

    @Override
    public boolean setExposicaoEmSubmissao() {
        return false;
    }

    @Override
    public boolean setExposicaoForaSubmissao() {
        return false;
    }

    @Override
    public boolean setExposicaoComConflitosDetetados() {
        return false;
    }

    @Override
    public boolean setExposicaoComConflitosDetetadosAlterados() {
        return false;
    }

    @Override
    public boolean setExposicaoComCandidaturasAtribuidas() {
        return false;
    }

    @Override
    public boolean setExposicaoComTodasCandidaturasAvaliadas() {
        return false;
    }

    @Override
    public boolean valida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoComFae() {
        return setExposicaoComFaeSemDemonstracoes();
    }
    
    @Override
    public boolean setExposicaoCompleta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoCriada() {
        return false;
    }
}
