package Tarefas;

import java.util.TimerTask;

public abstract class Tarefa extends TimerTask {

    @Override
    public abstract void run();
    
}
