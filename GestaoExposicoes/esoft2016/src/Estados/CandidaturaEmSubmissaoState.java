
package Estados;

import Dominio.Candidatura;

public class CandidaturaEmSubmissaoState implements CandidaturaState{
    
    private Candidatura candidatura;

    public CandidaturaEmSubmissaoState(Candidatura candidatura) {
        this.candidatura = candidatura;
    }

    @Override
    public boolean setAtribuida() {
        return false;
    }

    @Override
    public boolean setCandidaturaComConflitosDetetados() {
        return false;
    }

    @Override
    public boolean valida() {
        return false;
    }

    @Override
    public boolean setCandidaturaAvaliada() {
        return false;
    }

    @Override
    public boolean setCandidaturaSubmetida() {
        return false;
    }

    @Override
    public boolean setEmSubmissao() {
        if (valida()){
            CandidaturaEmAvaliacaoState ceaState = new CandidaturaEmAvaliacaoState(candidatura);
            candidatura.setEstado(ceaState);
            return true;
        }
        return false;
    }

    @Override
    public boolean setSubmissaoEncerrada() {
        return false;
    }

    @Override
    public boolean setEmAvaliacao() {
        return false;
    }

    @Override
    public boolean setNaoAvaliada() {
        return false;
    }

    @Override
    public boolean setRejeitada() {
        return false;
    }

    @Override
    public boolean setAceite() {
        return false;
    }

    @Override
    public boolean setRetirada() {
        if (valida()){
            CandidaturaRetiradaState crState = new CandidaturaRetiradaState(candidatura);
            candidatura.setEstado(crState);
            return true;
        }
        return false;
    }

    @Override
    public boolean setStandAtribuido() {
        return false;
    }

    @Override
    public boolean setStandEfetivo() {
        return false;
    }
    
    
    
}
