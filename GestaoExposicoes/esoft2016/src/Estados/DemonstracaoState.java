package Estados;

public interface DemonstracaoState {
    public boolean setCandidaturasAtribuidas();
    public boolean valida();
}
