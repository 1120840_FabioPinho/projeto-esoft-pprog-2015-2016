package Estados;

public class ExposicaoComConflitosAlteradosState implements ExposicaoState{

    @Override
    public boolean setExposicaoCriada() {
        return false;
    }

    @Override
    public boolean setExposicaoComFaeSemDemonstracoes() {
        return false;
    }

    @Override
    public boolean setExposicaoComDemonstracoesSemFae() {
        return false;
    }

    @Override
    public boolean setExposicaoCompleta() {
        return false;
    }

    @Override
    public boolean setExposicaoEmSubmissao() {
        return false;
    }

    @Override
    public boolean setExposicaoForaSubmissao() {
        return false;
    }

    @Override
    public boolean setExposicaoComConflitosDetetados() {
        return false;
    }

    @Override
    public boolean setExposicaoComConflitosDetetadosAlterados() {
        return true;
    }

    @Override
    public boolean setExposicaoComCandidaturasAtribuidas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoComTodasCandidaturasAvaliadas() {
        return false;
    }

    @Override
    public boolean valida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoComFae() {
        return false;
    }
    
}
