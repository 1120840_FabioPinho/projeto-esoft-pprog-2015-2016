package PersistentData;

import Dominio.Utilizador;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UtilizadoresFicheiro implements CarregarUtilizadores{

    public UtilizadoresFicheiro() {
    }
    
    @Override
    public List<Utilizador> carregaUtilizadores() {
        Scanner input;
        List <Utilizador> lstUtilizadores = new ArrayList<>();
        try {
            String basePath = new File("").getAbsolutePath();
            input = new Scanner(new File(basePath+"\\src\\dadosParaTestes\\utilizadores.txt"));
            input.useDelimiter(";|\n");
            int i = 0;
            while(input.hasNext()) {
                String nome = input.next();
                String email = input.next();
                String username = input.next();
                String password = input.next();
                Utilizador u = new Utilizador();
                u.setNome(nome);
                u.setEmail(email);
                u.setUsername(username);
                u.setPassword(password);
                lstUtilizadores.add(u);
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Ficheiro Não Encontrado!!");
        }
        return lstUtilizadores;
    }
}
