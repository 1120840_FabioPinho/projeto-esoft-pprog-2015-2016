
package Dominio;

import Estados.CandidaturaState;

public interface Candidatura {
    
    public boolean setCandidaturaSubmetida();
    public boolean setCandidaturaAvaliada(Candidatura candidatura);
    public boolean setEstado(CandidaturaState novoEstado);
    public boolean setEmSubmissao();
    public boolean setSubmissaoEncerrada();
    public boolean setEmAvaliacao();
    //public boolean setAvaliada();
    public boolean setNaoAvaliada();
    public boolean setRejeitada();
    public boolean setAceite();
    public boolean setRetirada();
    public boolean setStandAtribuido();
    public boolean setStandEfetivo();
    
}
