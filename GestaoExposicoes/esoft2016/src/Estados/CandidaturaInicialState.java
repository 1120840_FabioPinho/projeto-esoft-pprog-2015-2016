package Estados;

import Dominio.Candidatura;

public class CandidaturaInicialState implements CandidaturaState{

    private Candidatura candidatura;

    public CandidaturaInicialState(Candidatura candidatura) {
        this.candidatura = candidatura;
    }
    
    @Override
    public boolean setAtribuida() {
        return false;
    }

    @Override
    public boolean setCandidaturaComConflitosDetetados() {
        return false;
    }

    @Override
    public boolean valida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setCandidaturaAvaliada() {
        return false;
    }

    @Override
    public boolean setCandidaturaSubmetida() {
        if (valida()){
            CandidaturaEmSubmissaoState cesState = new CandidaturaEmSubmissaoState(candidatura);
            candidatura.setEstado(cesState);
            return true;
        }
        return false;
    }

    @Override
    public boolean setEmSubmissao() {
        return false;
    }

    @Override
    public boolean setSubmissaoEncerrada() {
        return false;
    }

    @Override
    public boolean setEmAvaliacao() {
        return false;
    }

    @Override
    public boolean setNaoAvaliada() {
        return false;
    }

    @Override
    public boolean setRejeitada() {
        return false;
    }

    @Override
    public boolean setAceite() {
        return false;
    }

    @Override
    public boolean setRetirada() {
        return false;
    }

    @Override
    public boolean setStandAtribuido() {
        return false;
    }

    @Override
    public boolean setStandEfetivo() {
        return false;
    }
    
}
