package Main;

import Controller.InicializaDadosAplicacaoController;
import UI.MainMenu;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GestaoExposicoes
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        InicializaDadosAplicacaoController inicializa = new InicializaDadosAplicacaoController();
        inicializa.carregaUtilizadores();
        inicializa.carregaExposicoes();
        
        MainMenu mainMenu = new MainMenu();

        try {
            mainMenu.run();
        } catch (IOException ex) {
            Logger.getLogger(GestaoExposicoes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}