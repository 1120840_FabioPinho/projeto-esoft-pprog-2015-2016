
package Tarefas;

import ControllersTarefas.SetEstadoExposicaoEmSubmissaoController;
import Dominio.Exposicao;

public class SetEstadoExposicaoEmSubmissao extends Tarefa{
    private SetEstadoExposicaoEmSubmissaoController seeesController;
    Exposicao exposicao;

    public SetEstadoExposicaoEmSubmissao(Exposicao exposicao) {
        this.exposicao = exposicao;
    }

    @Override
    public void run() {
        seeesController = new SetEstadoExposicaoEmSubmissaoController();
        seeesController.setEstadoExposicaoEmSubmissao(exposicao);
    }
    
}
