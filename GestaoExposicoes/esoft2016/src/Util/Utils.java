package Util;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Utils
{
    static public String readLineFromConsole(String strPrompt){
        try
        {
            System.out.println(strPrompt);

            InputStreamReader converter = new InputStreamReader(System.in);
            BufferedReader in = new BufferedReader(converter);

            return in.readLine();
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    static public Date readDateFromConsole(String strPrompt){
        do
        {
            try
            {
                String strDate = readLineFromConsole(strPrompt);

                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

                Date date = df.parse(strDate);

                return date;
            } catch (ParseException ex)
            {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (true);
    }
    
    static public boolean confirma(String sMessage){
        String strConfirma;
        do {
            strConfirma = Utils.readLineFromConsole("\n" + sMessage + "\n");
        } while (!strConfirma.equalsIgnoreCase("s") && !strConfirma.equalsIgnoreCase("n"));

        return strConfirma.equalsIgnoreCase("s");
    }
    
    static public Object apresentaESeleciona(List list,String sHeader){
        apresentaLista(list,sHeader);
        return selecionaObjecto(list);
    }
    
    static public void apresentaLista(List list,String sHeader){
        System.out.println(sHeader);

        int index = 0;
        for (Object o : list)
        {
            index++;

            System.out.println(index + ". " + o.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }
    
    static public Object selecionaObjecto(List list){
        String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > list.size());

        if (nOpcao == 0)
        {
            return null;
        } else
        {
            return list.get(nOpcao - 1);
        }
    }
    
    static public void limpaConsola(){
        try {
            Robot pressbot = new Robot();
            pressbot.keyPress(17); // Holds CTRL key.
            pressbot.keyPress(76); // Holds L key.
            pressbot.keyRelease(17); // Releases CTRL key.
            pressbot.keyRelease(76); // Releases L key.
            Thread.sleep(50);
        } catch (AWTException ex) {
            System.out.println("Erro ao tentar apagar a consola!!!");
        } catch (InterruptedException ex) {
            System.out.println("Não consegui por a dormir a thread");
        }
    }
    
    static public int apresentaListaSelecionaUm(List lista, String header){
        Utils.limpaConsola();
        int nOpcao;
        System.out.println(header);
        for(int i=0; i<lista.size(); i++){
            System.out.println((i+1) + "-" + lista.get(i));
        }
        System.out.println("0-Cancelar");
        
        do
        {
            String opcao;
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        } while (nOpcao < 0 || nOpcao > lista.size());
        return (nOpcao-1);
    }

    public static void sair() {
        System.exit(0);
    }
    
    public static boolean apresentaObjetoSolicitaConfirmacao(Object o, String msg){
        Utils.limpaConsola();
        System.out.println(msg);
        System.out.println(o.toString());
        return Utils.confirma("Confirma os dados apresentados??");
    }
}