package Estados;

import Dominio.CandidaturaExposicao;

public class CandidaturaComConflitosDetetadosState implements CandidaturaState{
    private CandidaturaExposicao candidatura;

    public CandidaturaComConflitosDetetadosState(CandidaturaExposicao candidatura) {
        this.candidatura = candidatura;
    }
    
    @Override
    public boolean setAtribuida() {
        if(valida()){
            CandidaturaEmAvaliacao caState = new CandidaturaEmAvaliacao();
            candidatura.setEstado(caState);
            return true;
        }
        return false;
    }

    @Override
    public boolean valida() {
        //valida se pode mudar para atribuida
        return true;
    }

    @Override
    public boolean setCandidaturaComConflitosDetetados() {
        return true;
    }

    @Override
    public boolean setCandidaturaAvaliada() {
        return false;
    }

    @Override
    public boolean setCandidaturaSubmetida() {
        return false;
    }

    @Override
    public boolean setEmSubmissao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setSubmissaoEncerrada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setEmAvaliacao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setNaoAvaliada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setRejeitada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setAceite() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setRetirada() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setStandAtribuido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setStandEfetivo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
