package UI;

import Util.Utils;
import java.io.IOException;

public class MainMenu {
    private String opcao;

    public MainMenu() {
    }

    public void run() throws IOException{
        LoginUI loginUI = new LoginUI();
        boolean userLoggedIn;
        boolean utilizadorQuerSair;
        do{
            loginUI.run();
            try{
                userLoggedIn = loginUI.loginTeveSucesso();
            }catch(NullPointerException ex){
                userLoggedIn = false;
                Utils.limpaConsola();
                System.out.println("Dados introduzidos incorretos");
            } 
        }while(!userLoggedIn);
        String username = loginUI.getUsername();
        
        do{
            //opcao = "1";
            Utils.limpaConsola();
            System.out.println("\n\n");
            System.out.println("1. Criar Exposição");
            System.out.println("2. Definir FAE");
            System.out.println("3. Atribuir Candidatura");
            System.out.println("4. Decidir Candidaturas");
            System.out.println("5. Registar Candidatura");
            System.out.println("6. Registar Utilizador");
            System.out.println("7. Confirmar Registo de Utilizador");
            
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            switch(opcao)
            {
                case "1":
                    CriarExposicaoUI ui1 = new CriarExposicaoUI();
                    ui1.run();
                    break;
                case "2":
                    DefinirFaeUI ui2 = new DefinirFaeUI(username);
                    ui2.run();
                    break;
                case "3":
                    AtribuirCandidaturasUI ui3 = new AtribuirCandidaturasUI(username);
                    ui3.run();
                    break;
                case "4":
//                    DecidirCandidaturasUI ui4 = new DecidirCandidaturasUI(m_ce, utilizador);
//                    ui4.run();
                    break;
                case "6":
                    CriarPerfilDeUtilizadorUI ui6 = new CriarPerfilDeUtilizadorUI();
                    ui6.run();
                    break;
                case "7":
//                    ConfirmarRegistoUtilizadorUI ui7 = new ConfirmarRegistoUtilizadorUI(m_ce, utilizador);
//                    ui7.run();
                    break;            }
        }
        while (!opcao.equals("0") );
    }
}

