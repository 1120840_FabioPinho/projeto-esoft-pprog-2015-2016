package UI;

import Controller.CriarExposicaoController;
import Util.Utils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CriarExposicaoUI {
    private CriarExposicaoController ceController;
    private List<String> lstUtilizadoresParaOrganizadores;
    public CriarExposicaoUI() {
        ceController =  new CriarExposicaoController();
    }
    
    public void run(){
        solicitaDadosExposicao();
        apresentaUtilizadores();
        apresentaDadosIntroduzidos();
    }
    private void solicitaDadosExposicao(){
        Utils.limpaConsola();
        String titulo  = Utils.readLineFromConsole("Introduza Título: ");
        String txtDescritivo = Utils.readLineFromConsole("Introduza Descritivo: ");
        String local = Utils.readLineFromConsole("Introduza Local: ");
        Date dtInicioExpo = Utils.readDateFromConsole("Introduza Data Inicio de Exposição (dd-mm-aaaa): ");
        Date dtFimExpo = Utils.readDateFromConsole("Introduza Data Fim de Exposição (dd-mm-aaaa): ");
        Date dtInicioSubmissao = Utils.readDateFromConsole("Introduza Data Inicio de Submissão de Candidaturas (dd-mm-aaaa): ");
        Date dtFimSubmissao = Utils.readDateFromConsole("Introduza Data Fim de Submissão de Candidaturas (dd-mm-aaaa): ");
        Date dtFimAlteracaoConflitos = Utils.readDateFromConsole("Introduza Data Fim de Alteração de Conflitos de Interesse (dd-mm-aaaa): ");
        ceController.setDadosExpo(titulo, txtDescritivo, local, dtInicioExpo, dtFimExpo, dtInicioSubmissao, dtFimSubmissao, dtFimAlteracaoConflitos);
    }
    
    private void apresentaUtilizadores(){
        int nrOrganizadores = 0;
        boolean adicionarMais = true;
        while(adicionarMais){
            do{
                Utils.limpaConsola();
                lstUtilizadoresParaOrganizadores = ceController.getUtilizadores();
                int index = Utils.apresentaListaSelecionaUm(lstUtilizadoresParaOrganizadores,"Lista de Utilizadores Disponiveis");
                    if(index == -1){
                        //cancelar
                        return;
                    }
                boolean adicionado = ceController.setOrganizador(index);
                if(adicionado){
                    nrOrganizadores ++;
                    System.out.println("Organizador adicionado");
                    Utils.readLineFromConsole("Prima Enter para continuar!!");
                }
                else{
                    System.out.println("Erro ao introduzir Organizador!!");
                    Utils.readLineFromConsole("Prima Enter para continuar!!");
                }
            }while(nrOrganizadores < 2);
            Utils.limpaConsola();
            adicionarMais = Utils.confirma("Deseja Adicionar mais organizadores??");    
        }
    }

    private void apresentaDadosIntroduzidos(){
        boolean confirma = Utils.apresentaObjetoSolicitaConfirmacao(ceController.getOrganizadores(),"Organizadores inseridos:");
        if(confirma){
            ceController.setListaOrganizadores();
        }
        confirma = Utils.apresentaObjetoSolicitaConfirmacao(ceController.getExposicao(),"Exposição Criada:");
        if(confirma){
            Utils.limpaConsola();
            boolean sucesso = ceController.registaExposicao();
            notificaSucesso(sucesso);
        }
    }
    
    private void notificaSucesso(boolean sucesso){
        if(sucesso){
            System.out.println("Sucesso");
        }
        else{
            System.out.println("ERRO: Exposição não guardada");
        }
        Utils.readLineFromConsole("prima Enter para continuar...");
        Utils.limpaConsola();
    }
}
