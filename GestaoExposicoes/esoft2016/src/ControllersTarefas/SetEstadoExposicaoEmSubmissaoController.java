package ControllersTarefas;

import Dominio.CentroDeExposicoes;
import Dominio.Exposicao;

public class SetEstadoExposicaoEmSubmissaoController {
    
    public SetEstadoExposicaoEmSubmissaoController() {
    }
    
    public void setEstadoExposicaoEmSubmissao(Exposicao exposicao) {
        exposicao.setExposicaoEmSubmissao();
    }
    
}
