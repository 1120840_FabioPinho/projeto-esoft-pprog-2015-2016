package UI;

import Controller.CriarPerfilDeUtilizadorController;
import Util.Utils;

public class CriarPerfilDeUtilizadorUI {
    
    private CriarPerfilDeUtilizadorController cpuController;
    
    public CriarPerfilDeUtilizadorUI(){
        cpuController = new CriarPerfilDeUtilizadorController();
    }
    
    public void run(){
        Utils.limpaConsola();
        System.out.println("Registar Utilizador:");
        solicitaDados();
        confirma();
    }

    private void solicitaDados() {
        String nome, email, username, password;
        nome = Utils.readLineFromConsole("Nome:");
        email = Utils.readLineFromConsole("Email:");
        username = Utils.readLineFromConsole("UserName:");
        password = Utils.readLineFromConsole("Password:");
        cpuController.defineUtilizador(nome, email, username, password);
    }

    private void confirma() {
        Utils.limpaConsola();
        System.out.println(cpuController.dadosUtilizadorIntroduzidos());
        boolean confirma = Utils.confirma("Confirma os Dados de Utilizador Introduzidos?");
        if(confirma){
            boolean sucesso = cpuController.registaUtilizador();
            notificaSucesso(sucesso);
        }
    }

    private void notificaSucesso(boolean sucesso) {
        if(sucesso){
            System.out.println("Sucesso");
        }
        else{
            System.out.println("ERRO: Utilizador não guardado");
        }
        Utils.readLineFromConsole("prima Enter para continuar...");
        Utils.limpaConsola();
    }
}