package Estados;

import Dominio.Exposicao;
import Dominio.ListaDemonstracoes;

public class ExposicaoCriadaState implements ExposicaoState{
    private Exposicao exposicao;
    
    public ExposicaoCriadaState(Exposicao expo) {
        this.exposicao = expo;
    }

    @Override
    public boolean setExposicaoCriada() {
        return true;
    }

    @Override
    public boolean setExposicaoComFaeSemDemonstracoes() {
        ExposicaoComFaeSemDemonstracoesState cfsdState = new ExposicaoComFaeSemDemonstracoesState(exposicao);
        exposicao.setEstado(cfsdState);
        return true;
    }

    @Override
    public boolean setExposicaoComDemonstracoesSemFae() {
        return false;
    }

    @Override
    public boolean setExposicaoCompleta() {
        ExposicaoCompletaState ecState = new ExposicaoCompletaState(exposicao);
        exposicao.setEstado(ecState);
        return true;
    }

    @Override
    public boolean setExposicaoEmSubmissao() {
        return false;
    }

    @Override
    public boolean setExposicaoForaSubmissao() {
        return false;
    }

    @Override
    public boolean setExposicaoComConflitosDetetados() {
        return false;
    }

    @Override
    public boolean setExposicaoComConflitosDetetadosAlterados() {
        return false;
    }

    @Override
    public boolean setExposicaoComCandidaturasAtribuidas() {
        return false;
    }

    @Override
    public boolean setExposicaoComTodasCandidaturasAvaliadas() {
        return false;
    }

    @Override
    public boolean setExposicaoComFae() {
        if(validaToCompleta()){
            return setExposicaoCompleta();
        }
        if(validaToComFaeSemDemo()){
            return setExposicaoComFaeSemDemonstracoes();
        }
        return false;
    }
    
    @Override
    public boolean valida() {
        return false;
    }

    private boolean validaToCompleta() {
        ListaDemonstracoes lstDemonstracoes = exposicao.getLstDemonstracoes();
        if(lstDemonstracoes.getNrDemonstracoes()>0){
            return true;
        }
        return false;
    }

    private boolean validaToComFaeSemDemo() {
        ListaDemonstracoes lstDemonstracoes = exposicao.getLstDemonstracoes();
        if(lstDemonstracoes.getNrDemonstracoes() == 0){
            return true;
        }
        return false;
    }
    
}
