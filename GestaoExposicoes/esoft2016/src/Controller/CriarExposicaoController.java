package Controller;

import Dominio.CentroDeExposicoes;
import Dominio.Exposicao;
import Dominio.ListaOrganizadores;
import Dominio.Organizador;
import Dominio.RegistoExposicoes;
import Dominio.RegistoUtilizadores;
import Dominio.Utilizador;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CriarExposicaoController {
    private CentroDeExposicoes centro;
    private RegistoExposicoes lstExposicoes;
    private RegistoUtilizadores lstUtilizadores;
    private Exposicao exposicao;
    private ListaOrganizadores lstTmpOrganizadores;
    
    public CriarExposicaoController() {
        centro = CentroDeExposicoes.getInstancia();
        lstExposicoes = centro.getRegistoExposicoes();
        exposicao = lstExposicoes.novaExposicao();
        lstTmpOrganizadores = exposicao.novaListaOrganizadores();
    }
    public void setDadosExpo(String titulo,String txtDescritivo, String local,Date dataInicioExpo,Date dataFimExpo,Date dataInicioSubmissao,Date dataFimSubmissao,Date dataFimAlteracaoConflitos){
        exposicao.setTitulo(titulo);
        exposicao.setTxtDescritivo(txtDescritivo);
        exposicao.setLocal(local);
        exposicao.setDataInicioExpo(dataInicioExpo);
        exposicao.setDataFimExpo(dataFimExpo);
        exposicao.setDataInicioSubmissao(dataInicioSubmissao);
        exposicao.setDataFimSubmissao(dataFimSubmissao);
        exposicao.setDataFimAlteracaoConflitos(dataFimAlteracaoConflitos);
    }
    
    public List getUtilizadores() {
        List<String> retorno = new ArrayList<>();
        lstUtilizadores = centro.getRegistoUtilizadores();
        List<Utilizador> listaUtilizadores = lstUtilizadores.getTodosUtilizadores();
        for (Utilizador utilizador : listaUtilizadores) {
            retorno.add(utilizador.getUsername());
        }
        return retorno;
    }
    
    public boolean setOrganizador(int index){
        boolean organizadorAdicionado = false;
        Utilizador utilizador = lstUtilizadores.get(index);
        Organizador organizador = lstTmpOrganizadores.novoOrganizador(utilizador);
        boolean organizadorValido = lstTmpOrganizadores.validaOrganizador(organizador);
        if(organizadorValido){
            organizadorAdicionado = lstTmpOrganizadores.adicionaOrganizador(organizador);
        }
        return organizadorValido&&organizadorAdicionado;
    }
    public boolean setListaOrganizadores(){
        List<Organizador> tmp = lstTmpOrganizadores.getTodosOrganizadores();
        return exposicao.setListaOrganizadores(tmp);
    }
    public boolean registaExposicao(){
        if(lstExposicoes.validaExposicao(exposicao)){
            return lstExposicoes.adicionaExposicao(exposicao);
        }
        return false;
    }

    public Exposicao getExposicao() {
        return exposicao;
    }

    public Object getOrganizadores() {
        return lstTmpOrganizadores;
    }

    public void setOrganizadores(ListaOrganizadores lstOrganizadores){
        List<Organizador> lstOrg = lstOrganizadores.getTodosOrganizadores();
        exposicao.setListaOrganizadores(lstOrg);
    }
}
