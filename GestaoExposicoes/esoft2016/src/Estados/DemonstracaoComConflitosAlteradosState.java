package Estados;

import Dominio.Demonstracao;

public class DemonstracaoComConflitosAlteradosState implements DemonstracaoState{

     private Demonstracao demonstracao;

    public DemonstracaoComConflitosAlteradosState(Demonstracao demonstracao) {
        this.demonstracao = demonstracao;
    }
    @Override
    public boolean setCandidaturasAtribuidas() {
        if(valida()){
            DemonstracaoEmSubmissaoState desState = new DemonstracaoEmSubmissaoState(demonstracao);
            demonstracao.setEstado(desState);
        }
        return false;
    }

    @Override
    public boolean valida() {
        return true;
    }
    
}
