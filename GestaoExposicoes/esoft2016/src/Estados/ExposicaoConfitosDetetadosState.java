package Estados;

public class ExposicaoConfitosDetetadosState implements ExposicaoState{

    @Override
    public boolean setExposicaoCriada() {
        return false;
    }

    @Override
    public boolean setExposicaoComFaeSemDemonstracoes() {
        return false;
    }

    @Override
    public boolean setExposicaoComDemonstracoesSemFae() {
        return false;
    }

    @Override
    public boolean setExposicaoCompleta() {
        return false;
    }

    @Override
    public boolean setExposicaoEmSubmissao() {
        return false;
    }

    @Override
    public boolean setExposicaoForaSubmissao() {
        return false;
    }

    @Override
    public boolean setExposicaoComConflitosDetetados() {
        return true;
    }

    @Override
    public boolean setExposicaoComConflitosDetetadosAlterados() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoComCandidaturasAtribuidas() {
        return false;
    }

    @Override
    public boolean setExposicaoComTodasCandidaturasAvaliadas() {
        return false;
    }

    @Override
    public boolean valida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean setExposicaoComFae() {
        return false;
    }
    
}
