package Tarefas;

import java.util.Date;
import java.util.Timer;

public abstract class Agendavel extends Timer{
    public abstract void schedule(Tarefa tarefa,Date dataExecucao);
}
